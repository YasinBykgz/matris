#include "matris.h"


ostream& operator<<(ostream& os, const matris k1){
	for (int i = 0; i<k1.p1; i++){
		os << endl;
		for (int j = 0; j<k1.p2; j++){
			os << k1.m1[i][j] << " ";
		}
	}
	os << endl;
	return os;
}
istream& operator>>(istream& is, matris& k1){
	for (int i = 0; i<k1.p1; i++){
		for (int j = 0; j<k1.p2; j++){
			is >> k1.m1[i][j];
		}
	}
	return is;
}
matris::matris(int a1, int a2){
	p1 = a1;
	p2 = a2;
	complexType aag;

	for(int i=0; i<p1; i++){
        for(int j=0; j<p2; j++){
            m1[i][j]=aag;
        }
	}
}
complexType* matris::operator[](int const index){
    if(index<=0 && index>p2){
        return &m1[0][index];
    }
}
matris matris::operator+(matris othermatris){
	matris t(p1, p2);
	for (int i = 0; i<p1; i++){
		for (int j = 0; j<p2; j++){
			t.m1[i][j] = m1[i][j] + othermatris.m1[i][j];
		}
	}
	cout<<"Matrislerin Toplami= "<<endl;
	return t;
}
matris operator*(matris o2, matris othermatris){
	matris B(2, 2);
	complexType aa, bb, cc;
	if (o2.p2 != othermatris.p1){
        cout<<"Bu Matrisler Carpilamaz!"<<endl;
	}
	else{
		for (int i = 0; i<o2.p1; i++){
			for (int j = 0; j<othermatris.p2; j++){
				for (int k = 0; k<o2.p2; k++){
					aa = o2.m1[i][k];
					bb = othermatris.m1[k][j];
					cc = aa * bb;
					B.m1[i][j] = B.m1[i][j] + cc;
				}
			}
		}
		cout<<"Matrislerin Carpimi= "<<endl;
		return B;
	}
}
matris matris::operator*(int v){
	for (int i = 0; i<this->p1; i++){
		for (int j = 0; j<this->p2; j++){
			this->m1[i][j] = this->m1[i][j] * v;
		}
	}
	cout<<"Skaler "<<v<<" sayisi ile carpimi= "<<endl;
	return *this;
}
matris matris::operator=(matris othermatris){
	if (this->p1 == othermatris.p1 && this->p2 == othermatris.p2){
		for (int i = 0; i<p1; i++){
			for (int j = 0; j<p2; j++){
				this->m1[i][j] = othermatris.m1[i][j];
			}
		}
	}
	return *this;
}

