#include "complexType.h"

complexType operator*(complexType& p1, double u){
	p1.realPart = u*p1.realPart;
	p1.imaginaryPart = u*p1.imaginaryPart;

	return p1;
}
ostream& operator<<(ostream& osobject, const complexType& complex){
	osobject << "(" << complex.realPart << "," << complex.imaginaryPart << ")";
	return osobject;
}
istream& operator>>(istream& isobject, complexType& complex){
	isobject >> complex.realPart >> complex.imaginaryPart;
	return isobject;
}
bool complexType::operator==(const complexType& othercomplex) const{
	return(realPart == othercomplex.realPart && imaginaryPart == othercomplex.imaginaryPart);
}

complexType::complexType(double real, double imag){
	realPart = real;
	imaginaryPart = imag;
}
void complexType::setComplex(const double& real, const double& imag){
	realPart = real;
	imaginaryPart = imag;
}
complexType complexType::operator+=(const complexType& otherComplex) const{
	complexType temp;
	temp.realPart = this->realPart + otherComplex.realPart;
	temp.imaginaryPart = this->imaginaryPart + otherComplex.imaginaryPart;
	return temp;
}
complexType complexType::operator+(const complexType& othercomplex) const{
	complexType temp;
	temp.realPart = realPart + othercomplex.realPart;
	temp.imaginaryPart = imaginaryPart + othercomplex.imaginaryPart;
	return temp;
}
complexType complexType::operator*(const complexType& othercomplex) const{
	complexType temp;
	temp.realPart = (this->realPart * othercomplex.realPart) - (this->imaginaryPart * othercomplex.imaginaryPart);
	temp.imaginaryPart = (this->realPart * othercomplex.imaginaryPart) + (this->imaginaryPart * othercomplex.realPart);
	return temp;
}

