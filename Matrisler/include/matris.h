#ifndef MATRIS_H
#define MATRIS_H
#include "complexType.h"
#include <iostream>

class matris
{
    friend ostream& operator<<(ostream& os, const matris k1);
	friend istream& operator>>(istream& is, matris& k1);
	friend matris operator*(matris o2, matris othermatris);

public:
	void setmatris(int, int);
	matris(int, int);
	matris();
	matris operator+(matris othermatris);
	matris operator*(int v);
	matris operator=(matris othermatris);
	complexType* operator[](int const index);
	complexType m1[5][5];
	int p1;
	int p2;
};

#endif // MATRIS_H
