#ifndef COMPLEXTYPE_H
#define COMPLEXTYPE_H
#include <iostream>
using namespace std;


class complexType
{
    friend ostream& operator<<(ostream& os, const complexType& p1);
	friend istream& operator>>(istream& is, complexType& p1);
	friend complexType operator*(complexType& p1, double);
public:
	void setComplex(const double& real, const double& imag);
	complexType(double real = 0, double imag = 0);
	complexType operator+(const complexType& otherComplex) const;
	complexType operator*(const complexType& otherComplex) const;
	bool operator==(const complexType& otherComplex) const;
	complexType operator+=(const complexType& otherComplex) const;

private:
	double realPart;
	double imaginaryPart;

};

#endif // COMPLEXTYPE_H
